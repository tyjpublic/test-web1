<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div>
		用户名：<input id="userNo" name="userNo" type="text"/>
		<br/>
		密码：<input id="password" name="password" type="password"/>
	</div>
	<div>
		<input type="button" value="登录" id="user_login"/>
	</div>
</body>
<script type="text/javascript" src="<c:url value='/resources/js/jquery.min.js'/>" ></script>
<script type="text/javascript">
	$(function(){
		$("#user_login").click(function(){
			$.ajax({
				type:"POST",
				data:{"userNo":$("#userNo").val(),"password":$("#password").val()},
				url:"<c:url value='/user/login'/>",
				success:function(data){
					alert(data);
				}
			})
		});
	})
</script>
</html>