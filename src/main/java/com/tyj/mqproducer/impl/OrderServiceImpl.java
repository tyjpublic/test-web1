package com.tyj.mqproducer.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.tyj.mqproducer.OrderService;

/**
 * @classname OrderServiceImpl
 * @description 
 * @author TangYaJun
 * @date 2019年1月5日 上午9:50:54
 */
@Service
public class OrderServiceImpl implements OrderService {
	
	@Resource
	private MqProducerImpl mqProducer;

	@Override
	public String mqTest(String sendMsg) {
		mqProducer.sendDataToQueue("order", sendMsg);
		return "Success";
	}

}
