package com.tyj.mqproducer.impl;

import javax.annotation.Resource;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class MqProducerImpl {
	
	@Resource
	private AmqpTemplate amqpTemplate;
	
	//公共入队方法
    public void sendDataToQueue(String queueKey, Object object) {
        try {
            amqpTemplate.convertAndSend(queueKey, object);
        } catch (Exception e) {
            log.error(e.toString());
        }

    }


}
