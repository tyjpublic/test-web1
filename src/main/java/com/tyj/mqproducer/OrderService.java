package com.tyj.mqproducer;

/**
 * @classname OrderService
 * @description 测试mq
 * @author TangYaJun
 * @date 2019年1月5日 上午9:49:48
 */
public interface OrderService {

	String mqTest(String sendMsg);
}
