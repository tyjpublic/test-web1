package com.tyj.quartz;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @classname RobotTask
 * @description 机器人定时任务（日报提醒）
 * @author TangYaJun
 * @date 2018年11月29日 上午9:16:09
 */
@Slf4j
public class RobotTask {
	
	public static String WEBHOOK_TOKEN = "https://oapi.dingtalk.com/robot/send?access_token=9b9d2e4525db84c6671e7ae63aba6131cb932fd26c63954aa344d5e660047cb5";
	
	public void work0(){
		String textMsg = "{ \"msgtype\": \"text\", \"text\": {\"content\": \"日报！日报！\"}}";
		sendMessage(textMsg);
	}
	
	public void work1(){
		try {
			String textMsg = "{ \"msgtype\": \"text\", \"text\": {\"content\": \"打卡！打卡！\"}}";
			sendMessage(textMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void work2(){
		try {
			String textMsg = "{ \"msgtype\": \"text\", \"text\": {\"content\": \"打卡！打卡！\"}}";
			sendMessage(textMsg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void sendMessage(String textMsg){
		try {
			HttpClient httpclient = HttpClients.createDefault();
			 
	        HttpPost httppost = new HttpPost(WEBHOOK_TOKEN);
	        httppost.addHeader("Content-Type", "application/json; charset=utf-8");
	 
	        StringEntity se = new StringEntity(textMsg, "utf-8");
	        httppost.setEntity(se);
	 
	        HttpResponse response;
			
			response = httpclient.execute(httppost);
			
	        if (response.getStatusLine().getStatusCode()== HttpStatus.SC_OK){
	            String result= EntityUtils.toString(response.getEntity(), "utf-8");
	            log.error(result);
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
