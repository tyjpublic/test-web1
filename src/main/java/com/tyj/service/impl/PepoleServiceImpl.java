package com.tyj.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.StringUtils;
import com.tyj.entity.Pepole;
import com.tyj.repository.PepoleMapper;
import com.tyj.service.PepoleService;

@Service
public class PepoleServiceImpl implements PepoleService {
	
	@Autowired
	private PepoleMapper pepoleMapper;

	@Override
	public Pepole getById(String id) {
		Pepole p = null;
		if(!StringUtils.isNullOrEmpty(id)){
			p = pepoleMapper.selectByPrimaryKey(Long.valueOf(id));
		}
		return p;
	}

	@Override
	@Transactional
	public String savePepole(Pepole p) throws Exception {
		Boolean flag = true;
		if(p != null){
			pepoleMapper.insert(p);
		}
		if(flag){
			throw new Exception();
		}
		return "SUCCESS";
	}

}
