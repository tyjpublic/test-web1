package com.tyj.service;

import com.tyj.entity.Pepole;

/**
 * @classname PepoleService
 * @description 人员接口
 * @author TangYaJun
 * @date 2019年1月7日 下午2:48:40
 */
public interface PepoleService {
	
	/**
	 * @Description 
	 * @param id
	 * @return
	 * @author TangYaJun
	 * @date 2019年1月7日 下午2:48:36
	 */
	Pepole getById(String id);

	/**
	 * @Description 
	 * @param p 新增人员
	 * @return
	 * @author TangYaJun
	 * @date 2019年1月7日 下午3:35:46
	 */
	String savePepole(Pepole p) throws Exception ;

}
