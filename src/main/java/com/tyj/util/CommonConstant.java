package com.tyj.util;

import java.io.Serializable;

/**
 * @classname CommonConstant
 * @description 系统常量
 * @author TangYaJun
 * @date 2018年12月29日 上午9:53:51
 */
public class CommonConstant implements Serializable {

	private static final long serialVersionUID = -1940095990546430729L;
	
	public static String userName;

	public static String getUserName() {
		return userName;
	}

	public static void setUserName(String userName) {
		CommonConstant.userName = userName;
	}

}
