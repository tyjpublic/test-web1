package com.tyj.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tyj.entity.Pepole;
import com.tyj.mqproducer.OrderService;
import com.tyj.service.PepoleService;
import com.tyj.util.CommonConstant;

@Controller
@RequestMapping("/user/")
public class LoginController {
	
	@Resource
	private OrderService orderService;
	
	@Resource
	private PepoleService pepoleService;
	
	@RequestMapping("/index")
	public String index(){
		return "index";
	}
	
	@RequestMapping("/login")
	@ResponseBody
	public String login(String userNo, String password){
		System.out.println("配置文件名称："+CommonConstant.getUserName());
		return CommonConstant.getUserName().equals(userNo) && "123".equals(password) ? "登录成功！":"登录失败！";
	}
	
	@RequestMapping("/mq")
	@ResponseBody
	public String mq(String sendMsg){
		return orderService.mqTest("你好！");
	}
	
	@RequestMapping("/pepole")
	@ResponseBody
	public String pepole(String id){
		return pepoleService.getById(id).toString();
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public String add(Pepole p){
		try {
			pepoleService.savePepole(p);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	

}
