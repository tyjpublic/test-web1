package com.tyj.repository;

import org.springframework.stereotype.Repository;

import com.tyj.entity.Pepole;

@Repository
public interface PepoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Pepole record);

    int insertSelective(Pepole record);

    Pepole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Pepole record);

    int updateByPrimaryKey(Pepole record);
}